/**
 * Vendor - Move to different file (problem with commonchunks on carte blance)
 */
// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';

if ('production' === ENV) {
  // Production
} else {
  // Development
}
