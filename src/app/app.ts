/*
 * Angular 2 decorators and services
 */
import {Component, OnInit} from '@angular/core';

/*
 * App Component
 * Top Level Component
 */
@Component({
    // The selector is what angular internally uses
    selector: 'cb-app', // <app></app>
    // The template for our app
    template: `<cb-card [name]="name" [description]="description" [image]="image"></cb-card>
    `
})
export class AppComponent implements OnInit {
    name: string;
    counter: number;
    description: string;
    image: string;
    isRed: boolean;

    constructor() {
        this.name = 'Angular2 Minimal';
        this.counter = 0;
        this.description = 'Hot reloading makes for a great Developer Experience, but we can do even better.';
        this.image = 'http://localhost/groovy/dist/images/dashboardbanner.jpg';
        this.isRed = true;
    }

    incrementCounter() {
        let newCounter = this.counter + 1;
        this.counter = newCounter;
    }

    ngOnInit() {
        // Our API
    }
}
