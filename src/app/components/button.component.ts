/*
 * Angular 2 decorators and services
 */
import {Component, Input} from '@angular/core';

/*
 * Name component
 */
@Component({
    selector: 'cb-button', // <name></name>
    styles: [`
    button {´
        font-family: helvetica;
        font-size: 11px;
        text-transform:uppercase;
        background: #24c775;
        padding: 10px; 20px;
        box-shadow: 1px 0px 3px rgba(0,0,0,0.5);
        border: none;
        border-radius: 2px;
        color: white;
    }

    .red {
        background: red;
    }
    `],
    // The template for our name component
    template: `
    <button class="button" (click)="alertMe();" [ngClass]="{'red': isRed}">{{text}}</button>
    `
})
export class ButtonComponent {
    @Input() text: string;
    @Input() isRed: boolean;

    constructor() {}

    alertMe() {
        alert('Alert from a generic button');
    }
}
