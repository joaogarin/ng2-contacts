/*
 * Angular 2 decorators and services
 */
import {Component, Input} from '@angular/core';

/*
 * Photo component
 */
@Component({
    selector: 'cb-photo', // <name></name>
    styles: [`
    img{
        width: 200px;
        height: auto;
    }
    `],
    // The template for our name component
    template: `
    <img alt="" [src]="imageSrc"/>
    `
})
export class PhotoComponent {
    @Input() imageSrc: string;

    constructor() {
    }
}
