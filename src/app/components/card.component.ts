/*
 * Angular 2 decorators and services
 */
import {Component, Input} from '@angular/core';

/*
 * Name component
 */
@Component({
    selector: 'cb-card', // <name></name>
    styles: [`
    .card{
        max-width: 250px;
        box-shadow: 1px 1px 3px 0px rgba(0,0,0,0.6);
        border-radius: 3px;
        font-family: helvetica;
        overflow: hidden;
    }
    .card-image-wrap {
        position: relative;
        width: 100%;
        overflow: hidden;
    }
    .card-image {
        max-width: 100%;
    }
    .card-text-wrap {
        padding: 20px;
        background: white;
    }
    .card-text {
        margin-bottom: 20px;
        font-size: 14px;
        text-transform: uppercase;
    }
    .card-description {
        font-size: 13px;
        line-height: 20px;
        color: #ccc;
        padding-top: 0px;
    }
    `],
    // The template for our name component
    template: `
    <div class="card">
        <div class="card-image-wrap">
            <img class="card-image" [src]="image"/>
        </div>
        <div class="card-text-wrap">
            <div class="card-text">{{name}}</div>
            <div class="card-description">{{description}}</div>
        </div>
    </div>
    `
})
export class CardComponent {
    @Input() name: string;
    @Input() description: string;
    @Input() image: string;

    constructor() {
    }
}
