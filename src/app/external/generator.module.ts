/*
 * Angular 2 decorators and services
 */
import { NgModuleFactory, ModuleWithProviders, NgModule, Component, Input, ElementRef, ReflectiveInjector, OnInit, NgZone, ComponentRef, ViewContainerRef, Compiler } from '@angular/core';
import { COMPILER_PROVIDERS } from '@angular/compiler';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

/*
 * Name component
 */
@Component({
    selector: 'cb-wrapper', // <name></name>
    styles: [``],
    // The template for our name component
    template: `
    <div id="dynamic-cmp" class="dynamic-comp"></div>
    `
})
export class GeneratorComponent implements OnInit {
    text: string = '';
    isRed: boolean = false;
    componentName: string;
    componentStr: string;

    cmpRef: ComponentRef<any>;

    constructor(private _ref: ElementRef, private ngZone: NgZone, private compiler: Compiler, private vcRef: ViewContainerRef) {
        
    }

    ngOnInit() {
        this.ngZone.run(() => {
            let nativeElement: HTMLElement = this._ref.nativeElement;
            this.componentName = nativeElement.getAttributeNode('data-component').value;
            // Inject thi button in this component and this component in te app

            // Root module was saved on the window Object (might find a better solution)
            let rootModule = (<any>window).ROOT;

            // Get the module and then get a hold of the component factory to inject
            this.compiler.compileModuleAndAllComponentsAsync(rootModule).then((moduleWithComponentFactory) => {
                const compFactory = moduleWithComponentFactory.componentFactories
                    .find(x => x.selector === this.componentName);
                this.vcRef.clear();
                const injector = ReflectiveInjector.fromResolvedProviders([], this.vcRef.parentInjector);
                this.cmpRef = this.vcRef.createComponent(compFactory, -1, injector, []);

                // Start building the component string
                let inputs = JSON.parse(nativeElement.getAttributeNode('data-inputs').value);
                let inputsStr = '';
                Object.keys(inputs).forEach(key => {
                    inputsStr += ` [${key}]='${inputs[key]}'`;
                    this.cmpRef.instance[key] = inputs[key];
                });

                // Save the string
                this.componentStr = `<${this.componentName} ${inputsStr}></${this.componentName}>`;
            });
        });
    }
}

@NgModule({
    declarations: [GeneratorComponent],
    imports: [BrowserModule],
    providers: [...COMPILER_PROVIDERS],
    bootstrap: [GeneratorComponent],
})
export class GeneratorModule {
    static forRoot(rootModule: any): ModuleWithProviders {
        (<any>window).ROOT = rootModule;
        return {
            ngModule: GeneratorModule,
        };
    }
}
platformBrowserDynamic().bootstrapModule(GeneratorModule);
