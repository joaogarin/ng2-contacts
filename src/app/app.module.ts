/**
 * Polyfills and vendors
 */
var polyfills = require('./../polyfills');
var vendor = require('./../vendor');

/*
 * Providers provided by Angular
 */
import { enableProdMode } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { COMPILER_PROVIDERS } from '@angular/compiler';

/*
 * App Component
 * our top level component that holds all of our components
 */
import { AppComponent } from './app';

/**
 * Import app specific components to declare
 */

import { ButtonComponent } from './components/button.component.ts';
import { CardComponent } from './components/card.component.ts';
import { PhotoComponent } from './components/photo.component.ts';

import { GeneratorModule } from './external/generator.module';

/*
 * App Environment Providers
 * providers that only live in certain environment
 */
if ('production' === process.env.ENV) {
    enableProdMode();
}

@NgModule({
    declarations: [AppComponent, ButtonComponent, CardComponent, PhotoComponent], // directives, components, and pipes owned by this NgModule
    imports: [
        BrowserModule, 
        GeneratorModule.forRoot(MyAppModule)
    ],
    providers: [
    ], // additional providers
    bootstrap: [AppComponent],
})
export class MyAppModule { }
