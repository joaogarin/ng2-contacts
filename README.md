# Angular 2 webpack minimal startekit

A working demo of [angular2] using [Webpack]

## Run the example

```bash
$ npm install
$ npm start & open http://localhost:3000
```

## View carteblanche

$ npm start & open localhost:3000/carte-blanche

## License

[MIT]

[Webpack]: http://webpack.github.io
[MIT]: http://markdalgleish.mit-license.org
[angular2]: http://angular.io